import {
    Button,
    Menu, MenuContent, MenuItem, MenuList,
    Card,
    CardBody,
    CardFooter,
    Modal,
    ModalVariant,
    Page,
    PageSection, Switch, Title,
    TreeView
} from "@patternfly/react-core";
import CultivationArea from "../components/3D/CultivationArea";
import { useEffect, useState } from "react";
import { TreeViewDataItem } from "@patternfly/react-core";
import { Container } from "../components/3D/model/Container";
import { Element } from "../components/3D/model/Element";
import { Plant } from "../components/3D/model/Plant";
import { Sensor } from "../components/3D/model/Sensor";
import API from "../services";
import "./DevicePage.css";

export const DevicePage = () => {
    const [getSensorDataList, setSensorDataList] = useState([]);
    const [getSwitchesRule, setSwitchesRule] = useState([]);
    const [getRules, setRules] = useState([]);
    const [getSensorList, setSensorList] = useState([]);
    const [getItem, setItem] = useState(null);
    const [switchStates, setSwitchStates] = useState({});
    const [isInvitationModalOpen, setIsInvitationModalOpen] = useState(false);
    const [getUsers, setUsers] = useState([]);
    const [getUsersToInvite, setUsersToInvite] = useState([]);
    const [selectedItems, setSelectedItems] = useState([]);

    const sensorMap = new Map();
    const url = window.location.href;
    const parts = url.split("/");
    const deviceId = parts[parts.length - 1];

  const selectItem = (
    event: React.MouseEvent,
    item: TreeViewDataItem,
    parentItem: TreeViewDataItem
  ) => {
    if (item instanceof Element) {
      setItem(item);
    }
  };

  const handleSwitchChange = async (
    _event,
    checked: boolean,
    deviceId,
    ruleId
  ) => {
    try {
      if (switchStates[ruleId]) {
        await API.removeRule(deviceId, ruleId)
          .then((response) => {
            response.data.forEach((rule) => {
              switchStates[rule.id] = false;
            });
            setSwitchesRule(switchesRules(response.data));
          })
          .catch((error) => {
            console.error(error);
          });
      } else {
        await API.addRule(deviceId, ruleId)
          .then((response) => {
            response.data.forEach((rule) => {
              switchStates[rule.id] = true;
            });
            setSwitchesRule(switchesRules(response.data));
          })
          .catch((error) => {
            console.error(error);
          });
      }
    } catch (error) {
      switchStates[ruleId] = !switchStates[ruleId];
    }
  };

  useEffect(() => {
    API.getSensors(deviceId)
      .then((response) => setSensorDataList(response.data))
      .catch(() => []);
    API.getRules()
      .then((response) => {
        response.data.forEach((rule) => {
          switchStates[rule.id] = false;
        });
        setRules(response.data);
      })
      .catch(() => []);
  }, []);

    useEffect(() => {
        generateInvitationList();
    }, [selectedItems]);

    useEffect(() => {
        setUsersToInvite(getUsers.map((user) => {
            if(user.isAdded) { setSelectedItems([...selectedItems, user.userId]); }
            return <MenuItem key={`user-${user.userId}`} itemId={user.userId} isSelected={selectedItems.indexOf(user.userId) !== -1}>{user.email}</MenuItem>
        }));
    }, [getUsers]);

    useEffect(() => {
        if(getRules.length > 0) {
            API.getActiveRules(deviceId)
                .then((response) => {
                    setSwitchesRule(switchesRules(response.data));
                    response.data.forEach((activeRule) => {
                        switchStates[activeRule.id] = true;
                    })
                })
                .catch((error) => {
                    console.error(error);
                })
        }
    }, [getRules]);

  useEffect(() => {
    if (getSensorDataList.length > 0) {
      sensorMap.set(
        "TEMPERATURE",
        new Sensor(
          "dht11_sensor",
          "DHT11 - Temperature",
          "http://localhost:3000/dht22.glb",
          0,
          0.1,
          0.1,
          0.1
        )
      );
      sensorMap.set(
        "HUMIDITY",
        new Sensor(
          "dht11_sensor",
          "DHT11 - Humidity",
          "http://localhost:3000/dht22.glb",
          0,
          0.1,
          0.1,
          0.1
        )
      );
    }
    getSensorDataList.forEach((sensor) => {
      const actualSensor = sensorMap.get(sensor.type.toUpperCase());
      actualSensor.valueSensor = sensor.valueSensor;
      actualSensor.completeId(sensor.id);
      sensorMap.set(sensor.type, actualSensor);
      setSensorList([...sensorMap.values()]);
    });
  }, [getSensorDataList]);

  const options = [
    {
      name: "Containers",
      id: "container_list",
      children: [
        {
          name: "Pots",
          id: "pot_variants",
          children: [
            new Container(
              "small_pot",
              "Small - 12L",
              "http://localhost:3000/small_pot.glb",
              0,
              1.5,
              1.5,
              1.5
            ),
          ],
        },
        {
          name: "Plots",
          id: "plot_variants",
          children: [
            new Container(
              "small_plot",
              "Small - (60X60)",
              "http://localhost:3000/plot.glb",
              1,
              0.145,
              0.1,
              0.08
            ),
            new Container(
              "medium_plot",
              "Medium - (120X100)",
              "http://localhost:3000/plot.glb",
              2,
              0.241,
              0.1,
              0.13
            ),
            new Container(
              "large_plot",
              "Large - (180X180)",
              "http://localhost:3000/plot.glb",
              3,
              0.338,
              0.1,
              0.234
            ),
          ],
        },
      ],
    },
    {
      name: "Plants",
      id: "plant_list",
      children: [
        new Plant(
          "tomato_plant",
          "Tomato",
          "http://localhost:3000/tomato_plant.glb",
          0,
          0.005,
          0.003,
          0.005
        ),
        new Plant(
          "corn_plant",
          "Corn",
          "http://localhost:3000/corn_plant.glb",
          0,
          0.5,
          0.25,
          0.5
        ),
      ],
    },
    {
      name: "Sensors",
      id: "sensor_list",
      children: getSensorList,
    },
  ];

  const switchesRules = (activeRules) => {
    return getRules.slice().map((rule) => {
      return (
        <Switch
          id={`rule_switch_${rule.id}`}
          label={`Rule ${rule.name}`}
          onChange={(_event, checked: boolean) =>
            handleSwitchChange(_event, checked, deviceId, rule.id)
          }
          isChecked={activeRules.some(
            (activeRule) => rule.id === activeRule.id
          )}
        />
      );
    });
  };

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

    const onSelect = (_event, itemId: number) => {
        if (selectedItems.indexOf(itemId) !== -1) {
            removeUserFromDevice(deviceId, itemId)
                .then(() => {
                    setSelectedItems(selectedItems.filter((id) => id !== itemId));
                });
        } else {
            addUserToDevice(deviceId, itemId)
                .then(() => {
                    setSelectedItems([...selectedItems, itemId]);
                })
        }
    };

    const handleInvitationModalToggle = () => {
        setIsInvitationModalOpen(!isInvitationModalOpen);
    };

    const generateInvitationList = () => {
        API.getUsersAvailableToInviteADevice(deviceId)
            .then((response) => {
                setUsers(response.data);
            })
            .catch(() => {
                setUsers([]);
            })
    }


    const addUserToDevice = (deviceId, email) => {
        return API.addUserToDevice(deviceId, email);
    }

    const removeUserFromDevice = (deviceId, email) => {
       return  API.removeUserFromDevice(deviceId, email);
    }

    return(
        <Page>
            <PageSection className={"view-cultivation-area"}>
                <div className="reference">
                    <img
                        /*  className="image" */
                        src={require("../resources/reference.png")}
                        alt="Reference"
                        onClick={handleModalToggle}
                    />
                </div>
                <div style={{ height: "inherit" }}>
                    <Button style={{marginLeft: '20px', backgroundColor: '#22a06b'}} onClick={() => setIsInvitationModalOpen(true)}>Inviting users to the device</Button>
                    <Title
                        headingLevel={"h6"}
                        children={"Insertables"}
                        style={{ padding: "20px" }}
                    />
                    <TreeView data={options} onSelect={selectItem} />
                    <Title
                        headingLevel={"h6"}
                        children={"Rules"}
                        style={{ padding: "20px" }}
                    />
                    {getSwitchesRule}
                </div>
                <CultivationArea selectedObject={getItem} />
                <Modal
                    variant={ModalVariant.small}
                    title="References"
                    isOpen={isModalOpen}
                    onClose={handleModalToggle}
                >
                    <div style={{ display: "flex", justifyContent: "center" }}>
                        <Card>
                            <CardBody>
                                <img
                                    src={require("../resources/left-mouse.png")}
                                    alt="Place object: Left click"
                                />
                            </CardBody>
                            <CardFooter>Place object: Left click</CardFooter>
                        </Card>
                        <Card>
                            <CardBody>
                                <img
                                    src={require("../resources/right-mouse.png")}
                                    alt="Remove object: Right click"
                                />
                            </CardBody>
                            <CardFooter>Remove object: Right click</CardFooter>
                        </Card>
                        <Card>
                            <CardBody>
                                <img
                                    src={require("../resources/inspect.png")}
                                    alt="Inspect object: Shift + left click"
                                />
                            </CardBody>
                            <CardFooter>Inspect object: Shift + left click</CardFooter>
                        </Card>
                    </div>
                </Modal>
                <Modal
                    title="Inviting users to the device"
                    isOpen={isInvitationModalOpen}
                    onClose={handleInvitationModalToggle}
                >
                    <Menu onSelect={onSelect} isScrollable>
                        <MenuContent>
                            <MenuList children={getUsersToInvite} />
                        </MenuContent>
                    </Menu>
                </Modal>
            </PageSection>
        </Page>
    )
}