import axios from "axios";

const baseURL = "http://localhost:8085";
const config = {
  headers: {
    Authorization: `Bearer ${localStorage.getItem("loginToken")}`,
    "Content-Type": "application/json",
  },
};

const API = {
  login: (email, password) =>
    axios.post(`${baseURL}/api/user/login`, {
      email: email,
      password: password,
    }),
  signUp: (email, password) =>
    axios.post(`${baseURL}/api/user/register`, {
      email: email,
      password: password,
    }),
  getSensors: (deviceId) =>
    axios.get(`${baseURL}/api/device/getSensors?device_id=${deviceId}`, config),
  getDevices: () => axios.get(`${baseURL}/api/device/devices`, config),
  getRules: () => axios.get(`${baseURL}/api/rule/getAll`, config),
  getActiveRules: (deviceId) => axios.get(`${baseURL}/api/device/activeRules?device_id=${deviceId}`, config),
  addRule: (deviceId, ruleId) => axios.post(`${baseURL}/api/device/addRule?device_id=${deviceId}&rule_id=${ruleId}`, {}, config),
  removeRule: (deviceId, ruleId) => axios.post(`${baseURL}/api/device/removeRule?device_id=${deviceId}&rule_id=${ruleId}`, {}, config),
  getNotifications: () => axios.get(`${baseURL}/api/notification/getAll`, config),
  markAsRead: (notificationId) => axios.post(`${baseURL}/api/notification/markAsRead?notification_id=${notificationId}`, {}, config),
  addUserToDevice: (deviceId, userId) => axios.post(`${baseURL}/api/device/addUser`, {deviceId: deviceId, userId: userId}, config),
  removeUserFromDevice: (deviceId, userId) => axios.post(`${baseURL}/api/device/removeUser`, {deviceId: deviceId, userId: userId}, config),
  getUsersAvailableToInviteADevice: (deviceId) => axios.get(`${baseURL}/api/device/availableUsers?device_id=${deviceId}`, config)
};

export default API;
