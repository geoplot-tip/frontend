# TIP GeoPlot - Frontend Project
GeoPlot is a web application that allows you to manage different crop plots in different parts of the world. The web client allows you to geo-locate a plot, obtain information from the sensors that make up the plot and generate customized profiles for different types of crops.

### Requirements:
- You must have NodeJs installed: https://nodejs.org/es

### Libraries used:
- Leaflet: https://leafletjs.com/
- Patternfly: https://www.patternfly.org/
- React-router: https://reactrouter.com/en/main
- Axios: https://axios-http.com/docs/intro

### Execution steps:
1. Open the terminal in the root directory of the project.
2. Execute the following command npm install
3. Finally, type and execute one of the following command lines:
    
* npm install -> Install the dependencies.
* npm start -> Start the project.
* npm test-> Launches the test runner in the interactive watch mode.
* npm run build -> Build the project.
* npm run eject -> Remove the single build dependency from your project.