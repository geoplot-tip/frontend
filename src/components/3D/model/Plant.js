import {Element} from "./Element";

export class Plant extends Element {
    isContainer() {
        return false;
    }

    isSensor() {
        return false;
    }

    isPlant() {
        return true;
    }

    canReceive(cultivationAreaElement) {
        return cultivationAreaElement.isSensor();
    }
}