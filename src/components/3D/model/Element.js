import {Object3D, Vector3} from "three";

export class Element {
    occupiedPositions: Vector3[] = [];
    distanceToOrigin: number;
    itemURL: string;
    scaleX: number;
    scaleY: number;
    scaleZ: number;
    name: string;
    id: string;
    position: Vector3;
    object3D: Object3D;
    value;

    constructor(id, name, itemURL, distanceToOrigin, scaleX, scaleY, scaleZ, value, position) {
        this.id = id;
        this.name = name;
        this.itemURL = itemURL;
        this.distanceToOrigin = distanceToOrigin;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.scaleZ = scaleZ;
        this.value = value;
        this.position = position;
    }

    setObject3D(object3D) {
        this.object3D = object3D;
    }

    generateOccupiedPositions = (target) => {
        const combinations: Array<Vector3> = [];

        for (let dx = -this.distanceToOrigin; dx <= this.distanceToOrigin; dx++) {
            for (let dz = -this.distanceToOrigin; dz <= this.distanceToOrigin; dz++) {
                const newX = target.x + dx;
                const newZ = target.z + dz;

                if (
                    Math.abs(newX - target.x) <= this.distanceToOrigin &&
                    Math.abs(newZ - target.z) <= this.distanceToOrigin &&
                    !(newX === target.x && newZ === target.z)
                ) {
                    combinations.push(new Vector3(newX, target.y, newZ));
                }
            }
        }
        combinations.push(target);
        this.occupiedPositions = combinations;
    }

    canReceive(cultivationAreaElement) {
        return false;
    };
    isContainer()  {
        return false;
    };
    isSensor()  {
        return false;
    };
    isPlant()  {
        return false;
    };
    completeId(id) {
        this.id = this.id + `_${id}`
    }
}