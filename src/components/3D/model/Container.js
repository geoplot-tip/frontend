import {Element} from "./Element";

export class Container extends Element {
    isContainer() {
        return true;
    }

    isSensor() {
        return false;
    }

    isPlant() {
        return false;
    }

    canReceive(cultivationAreaElement) {
        return cultivationAreaElement.isPlant() || cultivationAreaElement.isSensor();
    }
}