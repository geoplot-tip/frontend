import { Outlet, Navigate } from "react-router-dom";

const PrivateRoute = (props) => {
  let auth = localStorage.getItem("loginToken");
  return auth ? <Outlet /> : <Navigate to="/login" />;
};

export default PrivateRoute;
