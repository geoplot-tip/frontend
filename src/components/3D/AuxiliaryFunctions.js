import * as THREE from "three";
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import {Vector3} from "three";

const calculatePosition = (baseValue, gridSize, cellSize) => {
    return Math.floor((baseValue + gridSize / 2) / cellSize);
}

export const generateGround = (gridSize, cellSize, scene) => {
    const tileGroup = new THREE.Group();

    for (let x = -gridSize / 2 + cellSize / 2; x < gridSize / 2; x += cellSize) {
        for (let z = -gridSize / 2 + cellSize / 2; z < gridSize / 2; z += cellSize) {
            const isWhite = (calculatePosition(x, gridSize, cellSize) + calculatePosition(z, gridSize, cellSize)) % 2 === 0;

            const geometry = new THREE.BoxGeometry(cellSize, 0.1, cellSize);

            const material = isWhite
                ? new THREE.MeshBasicMaterial({ color: 0xffffff })
                : new THREE.MeshBasicMaterial({ color: 0x000000 });

            const tile = new THREE.Mesh(geometry, material);
            tile.position.set(x, 0, z);
            tileGroup.add(tile);
        }
    }
    scene.add(tileGroup);
    return tileGroup;
}

export const createCamera = (xPosition, yPosition, zPosition, fov, aspect) => {
    const camera = new THREE.PerspectiveCamera(fov, aspect, 0.1, 1000);
    camera.position.x = xPosition;
    camera.position.y = yPosition;
    camera.position.z = zPosition;
    return camera;
}

export const createLight = (xPosition, yPosition, zPosition, color, intensity, scene) => {
    scene.background = new THREE.Color(0xcccdfe);
    const directionalLight = new THREE.DirectionalLight(color, intensity);
    directionalLight.position.set(xPosition, yPosition, zPosition);
    scene.add(directionalLight);
}

export const createRenderer = (ref, width, height) => {
    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    ref.current.appendChild(renderer.domElement);
    return renderer;
}

export const createControls = (camera, renderer) => {
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.update();
}

export const removeElement = (event, renderer, mouse, raycaster, camera, ground, gridSize, cellSize) => {
    const canvasBounds = renderer.domElement.getBoundingClientRect();
    mouse.x = ((event.clientX - canvasBounds.left) / canvasBounds.width) * 2 - 1;
    mouse.y = -((event.clientY - canvasBounds.top) / canvasBounds.height) * 2 + 1;
    raycaster.setFromCamera(mouse, camera);
    const intersects = raycaster.intersectObjects(ground.children);

    if (intersects.length > 0) {
        const intersection = intersects[0];
        const position = intersection.point;

        const cellX = Math.floor((position.x + gridSize / 2) / cellSize);
        const cellZ = Math.floor((position.z + gridSize / 2) / cellSize);
        const positionX = cellX * cellSize - gridSize / 2 + cellSize / 2;
        const positionZ = cellZ * cellSize - gridSize / 2 + cellSize / 2;

        const tile = ground.children.find((tile) => tile.position.x === positionX && tile.position.z === positionZ);

        if (tile.children.length > 0) {
            const distances = tile.children.map((element) =>
                element.position.distanceTo(position)
            );

            const indexOfClosestElement = distances.indexOf(
                Math.min(...distances)
            );

            tile.children.splice(indexOfClosestElement, 1);
        }
    }
}
