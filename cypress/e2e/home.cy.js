describe("map", () => {
  it("should be markers on the map when plots are loaded", () => {
    cy.visit("localhost:3000/home");
    cy.wait(2000);

    cy.get(".leaflet-pane.leaflet-marker-pane > :nth-child(1)").should(
      "be.exist"
    );
    cy.get(".leaflet-pane.leaflet-marker-pane > :nth-child(2)").should(
      "be.exist"
    );
    cy.get(".leaflet-pane.leaflet-marker-pane > :nth-child(3)").should(
      "be.exist"
    );
  });

  it("should open a popup when the marker is clicked on", () => {
    cy.visit("localhost:3000/home");
    cy.wait(2000);
    cy.get(".leaflet-pane.leaflet-marker-pane > :nth-child(1)").click();
    cy.get(".leaflet-popup-content-wrapper").should("be.visible");
  });
});
