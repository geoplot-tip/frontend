import { Button } from "@patternfly/react-core";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import "./NotFound.css";

const NotFound = () => {
  useEffect(() => {
    if (!localStorage.getItem("loginToken")) {
      window.location.replace("/");
    }
  }, []);
  return (
    <div className="error">
      <p className="number"> 404 </p>
      <div className="text">
        <p>Oh! Page not found.</p>
        <p>The requested URL was not found on our server.</p>
      </div>
      <Link to="/home">
        <Button variant="danger" ouiaId="Danger">
          Go to the homepage
        </Button>
      </Link>
    </div>
  );
};

export default NotFound;
