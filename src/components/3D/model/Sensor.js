import {Element} from "./Element";

export class Sensor extends Element {

    isContainer() {
        return false;
    }

    isSensor() {
        return true;
    }

    isPlant() {
        return false;
    }

    canReceive(cultivationAreaElement) {
        return cultivationAreaElement.isSensor();
    }
}