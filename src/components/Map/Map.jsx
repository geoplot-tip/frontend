import "./Map.css";
import "leaflet/dist/leaflet.css";
import L from "leaflet";
import { useEffect, useRef } from "react";

const Map = (props) => {
  const mapContainer = useRef();

  const OnlineMarkerIcon = new L.Icon({
    iconUrl: require("../../resources/green-marker-icon.png"),
    iconSize: [34, 34],
  });

  const OfflineMarkerIcon = new L.Icon({
    iconUrl: require("../../resources/red-marker-icon.png"),
    iconSize: [34, 34],
  });

  const ProblemMarkerIcon = new L.Icon({
    iconUrl: require("../../resources/yellow-marker-icon.png"),
    iconSize: [34, 34],
  });

  useEffect(() => {
    const map = L.map(mapContainer.current, {
      attributionControl: false,
    }).setView(props.center, props.zoom);

    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);

    {
      props.visibility &&
        props.plots.map((plot, i) =>
          L.marker([plot.latitude, plot.longitude], {
            icon: plot.isActive ? (plot.hasProblems ? ProblemMarkerIcon : OnlineMarkerIcon) : OfflineMarkerIcon,
          })
            .addTo(map)
            .bindPopup(
              `Device name: ${plot.name} </br> 
               Device id: ${plot.id}. </br> 
               Latitude: ${plot.latitude}. </br> 
               Longitude: ${plot.longitude}. </br>
               ${extractValue(plot.sensorList)} </br>
               <a href="/device/${plot.id}">More Info</a>`
            )
        );
    }

    return () => map.remove();
  }, [props.plots, props.isOpen]);

  const extractValue = (sensorList) => {
    if (sensorList) {
      const sensorContent = sensorList
        .map((sensor) => `${sensor.type}: ${sensor.valueSensor}`)
        .join("<br/>");
      return "Sensors: " + "<br/>" + sensorContent + "<br/>";
    }
  };

  return (
    <div
      className={props.isOpen ? "map-reor" : "map"}
      ref={(el) => (mapContainer.current = el)}
    ></div>
  );
};

export default Map;
