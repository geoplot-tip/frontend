import {
  PageSection,
  Card,
  CardBody,
  CardFooter,
  Modal,
  ModalVariant,
} from "@patternfly/react-core";
import { useEffect, useState } from "react";

import Map from "../Map/Map";
import "./Home.css";
import { plots } from "../../config";
import API from "../../services";

const Home = (props) => {
  const [getPlots, setPlots] = useState([]);
  let isTesting = process.env.REACT_APP_TESTING.toLowerCase();
  useEffect(() => {}, [props.isOpen]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await API.getDevices();
        setPlots(response.data);
      } catch (error) {
        console.error("Error al realizar la solicitud GET:", error);
      }
    };

    if (isTesting === "true") {
      setPlots(plots);
    } else {
      fetchData();
    }

    const intervalId = setInterval(fetchData, 60000);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  return (
    <>
      <div className="reference">
        <img
          src={require("../../resources/reference.png")}
          alt="Reference"
          onClick={handleModalToggle}
        />
      </div>
      <Modal
        variant={ModalVariant.small}
        title="References"
        isOpen={isModalOpen}
        onClose={handleModalToggle}
      >
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Card>
            <CardBody>
              <img
                className="image"
                src={require("../../resources/green-marker-icon.png")}
                alt="Active"
              />
            </CardBody>
            <CardFooter>Active</CardFooter>
          </Card>
          <Card>
            <CardBody>
              <img
                className="image"
                src={require("../../resources/red-marker-icon.png")}
                alt="Inactive"
              />
            </CardBody>
            <CardFooter>Inactive</CardFooter>
          </Card>
          <Card>
            <CardBody>
              <img
                className="image"
                src={require("../../resources/yellow-marker-icon.png")}
                alt="With problems"
              />
            </CardBody>
            <CardFooter>With problems</CardFooter>
          </Card>
        </div>
      </Modal>
      <div className="container">
        <PageSection
          className={props.isOpen ? "map-container-reor" : "map-container"}
        >
          <Map
            center={[-34.753215, -58.207545]}
            zoom={12}
            scrool={true}
            plots={getPlots}
            visibility={true}
            isOpen={props.isOpen}
          ></Map>
        </PageSection>
      </div>
    </>
  );
};

export default Home;
