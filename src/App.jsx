import React, { useEffect } from "react";
import {
  Page,
  Masthead,
  MastheadToggle,
  MastheadMain,
  PageSidebar,
  PageSidebarBody,
  PageToggleButton,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
  Brand,
  MastheadContent,
  ToolbarGroup,
  Nav,
  NavList,
  NavItem,
  NotificationBadge,
  NotificationDrawer,
  NotificationDrawerBody,
  NotificationDrawerList,
  EmptyStateVariant,
  EmptyState,
  EmptyStateIcon,
  EmptyStateHeader,
  EmptyStateBody,
  NotificationBadgeVariant,
  DropdownItem,
  Dropdown,
  DropdownList,
  MenuToggle,
  NotificationDrawerHeader,
} from "@patternfly/react-core";
import BarsIcon from "@patternfly/react-icons/dist/esm/icons/bars-icon";
import Home from "./components/Home/Home";
import { BrowserRouter, Link, Navigate, Route, Routes } from "react-router-dom";
import NotFound from "./components/NotFound/NotFound";
import { DevicePage } from "./pages/DevicePage";
import logo from "./resources/logo.png";
import { RegisterLogin } from "./components/RegisterLogin/RegisterLogin";
import PrivateRoute from "./components/PrivateRoute ";
import { BellIcon, EllipsisVIcon, SearchIcon } from "@patternfly/react-icons";
import API from "./services";
import { Notification } from "./components/Notification/Notification";

const App = () => {
  const drawerRef = React.useRef(null);
  const [isSidebarOpen, setIsSidebarOpen] = React.useState(false);
  const [isDrawerExpanded, setIsDrawerExpanded] = React.useState(false);
  const [isUnreadMap, setIsUnreadMap] = React.useState({});
  const [shouldShowNotifications, setShouldShowNotifications] =
    React.useState(false);
  const [getNotifications, setNotifications] = React.useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await API.getNotifications()
          .then((value) => {
            const unreadNotifications = value.data.filter((notification) => {
              return !notification.read;
            });

            setNotifications(
              unreadNotifications.map((item) => {
                return (
                  <Notification
                    id={item.id}
                    variant={item.variant}
                    title={item.title}
                    content={item.content}
                    isRead={item.read}
                    markAsRead={API.markAsRead}
                  ></Notification>
                );
              })
            );

            setShouldShowNotifications(unreadNotifications.length > 0);
          })
          .catch((error) => {
            console.error(error);
          });
      } catch (error) {
        console.error("GET request failed: ", error);
      }
    };

    fetchData();

    const intervalId = setInterval(fetchData, 60000);

    return () => {
      clearInterval(intervalId);
    };
  }, [getNotifications]);

  const showNotifications = (showNotifications) => {
    setIsUnreadMap(null);
    setShouldShowNotifications(showNotifications);
  };

  const onCloseNotificationDrawer = () =>
    setIsDrawerExpanded((prevState) => !prevState);

  const getNumberUnread = () =>
    Object.values(isUnreadMap).reduce(
      (count, value) => count + (value ? 1 : 0),
      0
    );

  const onSidebarToggle = () => setIsSidebarOpen(!isSidebarOpen);

  const logout = () => {
    localStorage.removeItem("loginToken");
    window.location.replace("/");
  };

  const dropdownItems = (
    <>
      <DropdownItem>Marcar como leído</DropdownItem>
    </>
  );

  const [isOpen, setIsOpen] = React.useState(false);
  const onToggleClick = () => {
    setIsOpen(!isOpen);
  };
  const onSelect = (_event, value) => {
    console.log("selected", value);
    setIsOpen(false);
  };

  const headerToolbar = (
    <Toolbar id="vertical-toolbar">
      <ToolbarContent>
        <ToolbarGroup
          variant="icon-button-group"
          align={{
            default: "alignRight",
          }}
          spacer={{
            default: "spacerNone",
            md: "spacerMd",
          }}
        >
          <ToolbarItem
            visibility={{ default: "visible" }}
            selected={isDrawerExpanded}
            children={
              <NotificationBadge
                variant={
                  getNotifications.length === 0
                    ? NotificationBadgeVariant.read
                    : NotificationBadgeVariant.attention
                }
                onClick={onCloseNotificationDrawer}
                aria-label="Notifications"
                isExpanded={isDrawerExpanded}
                children={<BellIcon />}
              />
            }
          />
        </ToolbarGroup>
      </ToolbarContent>
    </Toolbar>
  );

  const header = (
    <Masthead>
      {
        <MastheadToggle>
          <PageToggleButton
            variant="plain"
            aria-label="Global navigation"
            isSidebarOpen={isSidebarOpen}
            onSidebarToggle={onSidebarToggle}
            id="vertical-nav-toggle"
          >
            <BarsIcon />
          </PageToggleButton>
        </MastheadToggle>
      }
      <MastheadMain>
        <Link to="/home" title="Home" className="link-router">
          <Brand
            src={logo}
            alt={"Geoplot Logo"}
            widths={{ default: "40px", sm: "60px", md: "220px" }}
          />
        </Link>
      </MastheadMain>
      <MastheadContent>{headerToolbar}</MastheadContent>
    </Masthead>
  );

  const sidebar = (
    <PageSidebar
      isSidebarOpen={isSidebarOpen}
      id="multiple-sidebar-body-sidebar"
    >
      <PageSidebarBody usePageInsets>
        <Nav>
          <NavList>
            <NavItem itemId={0} onClick={logout}>
              Logout
            </NavItem>
          </NavList>
        </Nav>
      </PageSidebarBody>
    </PageSidebar>
  );

  return (
    <BrowserRouter>
      <Page
        notificationDrawer={
          <NotificationDrawer ref={drawerRef}>
            <NotificationDrawerBody>
              {shouldShowNotifications && (
                <NotificationDrawerList>
                  {getNotifications}
                </NotificationDrawerList>
              )}
              {!shouldShowNotifications && (
                <EmptyState variant={EmptyStateVariant.full}>
                  <EmptyStateHeader
                    headingLevel="h2"
                    titleText="No alerts found"
                    icon={<EmptyStateIcon icon={SearchIcon} />}
                  />
                  <EmptyStateBody
                    children={"There are currently no notifications"}
                  />
                </EmptyState>
              )}
            </NotificationDrawerBody>
          </NotificationDrawer>
        }
        isNotificationDrawerExpanded={isDrawerExpanded}
        header={localStorage.getItem("loginToken") && header}
        sidebar={sidebar}
      >
        <Routes>
          <Route path="/">
            {!localStorage.getItem("loginToken") && (
              <Route index element={<Navigate to="/login" replace />} />
            )}
            {localStorage.getItem("loginToken") && (
              <Route index element={<Navigate to="/home" replace />} />
            )}

            {!localStorage.getItem("loginToken") && (
              <Route path="login" element={<RegisterLogin page="login" />} />
            )}

            {localStorage.getItem("loginToken") && (
              <Route path="login">
                <Route index element={<Navigate to="/home" replace />} />
              </Route>
            )}

            <Route
              path="auth/register"
              element={<RegisterLogin page="signup" />}
            />
            <Route element={<PrivateRoute />}>
              <Route path="home" element={<Home isOpen={isSidebarOpen} />} />
              <Route path={"device/:token"} element={<DevicePage />} />
              <Route path="*" element={<NotFound />} />
            </Route>
          </Route>
        </Routes>
      </Page>
    </BrowserRouter>
  );
};

export default App;
