import React, {useEffect, useRef, useState} from 'react';
import * as THREE from 'three';
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {Modal, TreeViewDataItem} from "@patternfly/react-core";
import {
    createCamera, createControls, createLight,
    createRenderer,
    generateGround, removeElement
} from "./AuxiliaryFunctions";
import {Vector3} from "three";
import {Sensor} from "./model/Sensor";

const ThreeScene = ({selectedObject} : {selectedObject: TreeViewDataItem}) => {
    const cellSize = 1;
    const gridSize = 11;
    const containerRef = useRef(null);
    const [getMouse, setMouse] = useState(null);
    const [getCamera, setCamera] = useState(null);
    const [getGround, setGround] = useState(null);
    const [getRenderer, setRenderer] = useState(null);
    const [getRaycaster, setRaycaster] = useState(null);
    const [getScene, setScene] = useState(null);
    const [creationIsComplete, setCreationIsComplete] = useState(false);
    const [actionIsComplete, setActionIsComplete] = useState(false);
    const [getRegisteredPositions, setRegisteredPositions] = useState([]);
    const [isModalOpen, setIsModalOpen] = React.useState(false);
    const [getSensorData, setSensorData] = React.useState(null);
    const value = useRef(null);

   /* Initialization */
    useEffect(() => {
        if(!creationIsComplete) {
            const scene = new THREE.Scene();
            const mouse = new THREE.Vector2();
            const raycaster = new THREE.Raycaster();
            const ground = generateGround(gridSize, cellSize, scene);
            const renderer = createRenderer(containerRef, 700, 500);
            const camera = createCamera(8, 5, 0, 100, 700 / 500);

            createLight(1, 1, 1, 0xffffff, 5, scene);
            createControls(camera, renderer);

            setMouse(mouse);
            setGround(ground);
            setCamera(camera);
            setRenderer(renderer);
            setRaycaster(raycaster);
            setScene(scene);

            const animate = () => {
                requestAnimationFrame(animate);
                renderer.render(scene, camera);
            };

            animate();

            setCreationIsComplete(true);

        } else {
            const threeScene = document.getElementById('three_scene');

            if (threeScene && threeScene.childElementCount > 0) {
                threeScene.removeChild(threeScene.firstChild);
            }
        }
    }, [creationIsComplete]);

    /* Interactions */
    useEffect(() => {
        if(creationIsComplete) {
            const onClick = (event) => {
                const isInspectMode = event.shiftKey;

                const canvasBounds = getRenderer.domElement.getBoundingClientRect();
                getMouse.x = ((event.clientX - canvasBounds.left) / canvasBounds.width) * 2 - 1;
                getMouse.y = -((event.clientY - canvasBounds.top) / canvasBounds.height) * 2 + 1;
                getRaycaster.setFromCamera(getMouse, getCamera);
                const loader = new GLTFLoader();
                const intersects = getRaycaster.intersectObjects(getGround.children);
                let element;
                let indexOfClosestElement;

                if (intersects.length > 0) {
                    const intersection = intersects[0];
                    const position = intersection.point;

                    const cellX = Math.floor((position.x + gridSize / 2) / cellSize);
                    const cellZ = Math.floor((position.z + gridSize / 2) / cellSize);

                    const positionX = cellX * cellSize - gridSize / 2 + cellSize / 2;
                    const positionZ = cellZ * cellSize - gridSize / 2 + cellSize / 2;

                    const tile = getGround.children.find(tile => tile.position.x === positionX && tile.position.z === positionZ);

                    if (tile.children.length > 0) {
                        const distances = tile.children.map((element) => element.position.distanceTo(position));
                        indexOfClosestElement = distances.indexOf(Math.min(...distances));
                    }

                    if(indexOfClosestElement) {
                        if(tile.children[indexOfClosestElement].animations[0] instanceof Sensor) {
                            const sensor = tile.children[indexOfClosestElement].animations[0];
                            setSensorData(`The sensor ${sensor.name} has sensed the following value ${sensor.valueSensor}`);
                        } else {
                            setSensorData("The element you are trying to inspect does not have any information.")
                        }
                        if (isInspectMode) {
                            setIsModalOpen(true);
                        }
                    }

                    !isInspectMode && loader.load(value.current.itemURL, (gltf) => {
                        const positionX = cellX * cellSize - gridSize / 2 + cellSize / 2;
                        const positionZ = cellZ * cellSize - gridSize / 2 + cellSize / 2;

                        const tile = getGround.children.find(tile => tile.position.x === positionX && tile.position.z === positionZ);

                        const positionY = tile.children.length > 0 ? 0.005 + tile.children.length * 0.6 : 0

                        value.current.generateOccupiedPositions(new Vector3(positionX, positionY, positionZ));

                        if (tile.children.length > 0) {
                            if(value.current.isContainer() && !value.current.occupiedPositions.some((position) => getRegisteredPositions.some((positionNew) => position.x === positionNew.x && position.z === positionNew.z))) {
                                element = gltf.scene.children[0];
                                element.scale.set(value.current.scaleX, value.current.scaleZ, value.current.scaleY);
                                element.position.set(positionX, positionY, positionZ);
                                element.animations.push(value.current);
                                tile.children.push(element);
                                setRegisteredPositions(getRegisteredPositions.push(value.current.occupiedPositions.shift()));
                            } else {
                                if(tile.children[indexOfClosestElement].animations[0].canReceive(value.current)) {
                                    element = gltf.scene.children[0];
                                    element.scale.set(value.current.scaleX, value.current.scaleZ, value.current.scaleY);
                                    element.position.set(positionX, positionY, positionZ);
                                    element.animations.push(value.current);
                                    tile.children.push(element);
                                    setRegisteredPositions(getRegisteredPositions.push(value.current.occupiedPositions.shift()));
                                }
                            }
                        } else {
                            if(value.current.isContainer()) {
                                value.current.occupiedPositions.forEach((position) => {
                                    element = gltf.scene.children[0];
                                    const copiedValue = { ...value.current };

                                    const tile = getGround.children.find(tile => tile.position.x === position.x && tile.position.z === position.z);
                                    element.position.set(position.x, position.y, position.z);

                                    if(position.z === positionZ && position.x === positionX && position.y === positionY) {
                                        element.scale.set(copiedValue.scaleX, copiedValue.scaleZ, copiedValue.scaleY);
                                    }
                                    copiedValue.position = position;
                                    element.animations.push(copiedValue);
                                    tile.children.push(element);
                                })
                                setRegisteredPositions(getRegisteredPositions.push(value.current.occupiedPositions.shift()));
                            }
                        }
                    });
                    }
                }

            if(!actionIsComplete) {
                const removeElementFromScene = (event) => { removeElement(event, getRenderer, getMouse, getRaycaster, getCamera, getGround, gridSize, cellSize) }
                window.addEventListener('contextmenu', removeElementFromScene);
                window.addEventListener('click', onClick);
                setActionIsComplete(true);
            }
        }
    }, [value.current, creationIsComplete]);

    useEffect(() => {
        value.current = selectedObject;
    }, [selectedObject]);

    return <div className={'framework-container'} id={'three_scene'} ref={containerRef}>
        <Modal
            variant={"small"}
            isOpen={isModalOpen}
            title={"Sensor Information"}
            children={getSensorData}
            onClose={() => setIsModalOpen(!isModalOpen)}
        />
    </div>;
}

export default ThreeScene;