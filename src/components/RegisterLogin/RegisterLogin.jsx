import React from "react";
import {
  Alert,
  LoginForm,
  LoginMainFooterBandItem,
  LoginPage,
} from "@patternfly/react-core";
import API from "../../services/API";
import logo from "../../resources/favicon.ico";
export const RegisterLogin = (props) => {
  const [showSuccess, setShowSuccess] = React.useState(false);
  const [showFail, setShowFail] = React.useState(false);
  const [messages, setMessages] = React.useState({});
  const [username, setUsername] = React.useState("");
  const [isValidUsername, setIsValidUsername] = React.useState(true);
  const [password, setPassword] = React.useState("");
  const [isValidPassword, setIsValidPassword] = React.useState(true);
  const handleUsernameChange = (_event, value) => {
    setUsername(value);
  };
  const handlePasswordChange = (_event, value) => {
    setPassword(value);
  };

  const onLoginButtonClick = (event) => {
    event.preventDefault();
    setIsValidUsername(true);
    setIsValidPassword(true);
    setShowSuccess(false);
    setShowFail(false);
    setMessages({});
    switch (props.page) {
      case "login":
        API.login(username, password)
          .then((response) => {
            localStorage.setItem(
              "loginToken",
              response.headers.authorization.slice(7)
            );
            window.location.replace("/home");
          })
          .catch((error) => {
            setIsValidUsername(false);
            setIsValidPassword(false);
            setMessages({ user: "Invalid credentials." });
            setShowFail(true);
          });
        break;
      default:
        API.signUp(username, password)
          .then(() => {
            setShowSuccess(true);
          })
          .catch((error) => {
            setMessages(error.response.data);
            setShowFail(true);
            Object.entries(error.response.data).forEach((error) => {
              if (error[0] === "password") {
                setIsValidPassword(false);
              } else setIsValidUsername(false);
            });
          });
        break;
    }
  };

  const signUpForAccountMessage =
    props.page === "login" ? (
      <LoginMainFooterBandItem>
        Need an account?{" "}
        <a href="http://localhost:3000/auth/register">Sign up.</a>
      </LoginMainFooterBandItem>
    ) : (
      <LoginMainFooterBandItem>
        Have an account? <a href="http://localhost:3000/">Log In.</a>
      </LoginMainFooterBandItem>
    );

  const loginForm = (
    <LoginForm
      usernameLabel="Email"
      usernameValue={username}
      onChangeUsername={handleUsernameChange}
      isValidUsername={isValidUsername}
      passwordLabel="Password"
      passwordValue={password}
      isShowPasswordEnabled
      onChangePassword={handlePasswordChange}
      isValidPassword={isValidPassword}
      onLoginButtonClick={onLoginButtonClick}
      loginButtonLabel={props.page === "login" ? "Log In" : "Sign Up"}
    />
  );

  return (
    <LoginPage
      brandImgSrc={logo}
      brandImgAlt="Geoplot logo"
      backgroundImgSrc="/assets/images/pfbg-icon.svg"
      textContent="Welcome to Geoplot. Please enter your credentials to continue."
      loginTitle={
        props.page === "login"
          ? "Log in to your account"
          : "Sign up to your account"
      }
      signUpForAccountMessage={signUpForAccountMessage}
    >
      {showSuccess && (
        <Alert
          variant="success"
          title="The user has been successfully created"
          ouiaId="SuccessAlert"
        />
      )}

      {showFail &&
        Object.entries(messages).map((m) => (
          <Alert variant="danger" title={m[1]} ouiaId="DangerAlert" />
        ))}
      {loginForm}
    </LoginPage>
  );
};
