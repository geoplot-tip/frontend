export const backendUrl = "http://localhost:8085";
export const userId = "jpc@gmail.com";
export const plots = [
  {
    id: 1,
    device_name: "Arduino-1",
    latitude: -34.72904,
    longitude: -58.26374,
    temperature: 20,
    humidity: 20,
    isActive: true,
    userList: [],
    sensorList: [],
    ruleList: []
  },
  {
    id: 2,
    device_name: "Arduino-2",
    latitude: -34.76531,
    longitude: -58.21278,
    isActive: true,
    userList: [],
    sensorList: [],
    ruleList: []
  },
  {
    id: 3,
    device_name: "Arduino-3",
    latitude: -34.71667,
    longitude: -58.3,
    temperature: 20,
    humidity: 20,
    isActive: true,
    userList: [],
    sensorList: [],
    ruleList: []
  },
];
