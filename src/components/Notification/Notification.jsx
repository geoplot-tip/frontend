import {
  Dropdown,
  DropdownItem,
  DropdownList,
  MenuToggle,
  NotificationDrawerListItem,
  NotificationDrawerListItemBody,
  NotificationDrawerListItemHeader,
} from "@patternfly/react-core";
import { EllipsisVIcon } from "@patternfly/react-icons";
import { useState } from "react";

export const Notification = ({
  id,
  variant,
  title,
  content,
  isRead,
  markAsRead,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const onToggleClick = () => {
    setIsOpen(!isOpen);
  };
  const onSelect = (_event, value) => {
    console.log("selected", value);
    setIsOpen(false);
  };

  return (
    <NotificationDrawerListItem
      variant={variant}
      isRead={isRead}
      /*  onClick={() => markAsRead(id)} */
    >
      <NotificationDrawerListItemHeader variant={variant} title={title}>
        <Dropdown
          popperProps={{
            position: "right",
          }}
          isOpen={isOpen}
          onSelect={onSelect}
          onOpenChange={(isOpen) => setIsOpen(isOpen)}
          toggle={(toggleRef) => (
            <MenuToggle
              ref={toggleRef}
              aria-label="kebab dropdown toggle"
              variant="plain"
              onClick={onToggleClick}
              isExpanded={isOpen}
            >
              <EllipsisVIcon />
            </MenuToggle>
          )}
          shouldFocusToggleOnSelect
        >
          <DropdownList>
            <DropdownItem value={0} key="action" onClick={() => markAsRead(id)}>
              Mark as read
            </DropdownItem>
          </DropdownList>
        </Dropdown>
      </NotificationDrawerListItemHeader>
      <NotificationDrawerListItemBody
        children={content}
        style={{ fontSize: "11px" }}
      />
    </NotificationDrawerListItem>
  );
};
